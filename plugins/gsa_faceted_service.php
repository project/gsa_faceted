<?php

/**
 * @file
 * GSA implementation for a service class which supports facets via Facet API.
 */

/**
 * Defines the "search_api_facets" feature for the GSA Faceted Search.
 */
class GsaFacetedService extends SearchApiAbstractService {

  /**
   * Skeleton method to tell Search API that we have indexed the items.
   *
   * Because the GSA works differently from other search platforms, Indexing
   * doesn't actually need to be done.  However, SearchAPI requires this
   * function, so we keep it in.
   *
   * The actual work that is done is simply to give back a full array of the
   * keys of the passed $items parameter.
   */
  public function indexItems(SearchApiIndex $index, array $items) {
    return (array_keys($items));
  }

  /**
   * Skeleton method for deleting items from the index.
   *
   * The GSA doesn't keep an index, so there is no way to delete the items from
   * the index.  However, SearchAPI requires there be a method of this name, so
   * we keep it here.
   */
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
  }


  /**
   * Helper function to get GSA module settings.
   *
   * @param string $key
   *   The setting key you want to recover.
   *
   * @return mixed
   *   Return an array of settings values, or a unique setting value if a key
   *   is provided.
   */
  public function getSettings($key = NULL) {
    $settings = _google_appliance_get_settings();

    if ($key == NULL) {
      return $settings;
    }
    elseif ($key && isset($settings[$key])) {
      return $settings[$key];
    }
    else {
      return FALSE;
    }
  }


  /**
   * Helper method to build the query conditions in GSA format.
   *
   * @param SearchApiQueryFilterInterface $filter
   *   The filters from the search query.
   * @param array $fields
   *   The internal field information to use.
   * @param SearchApiQueryInterface $query
   *   The SearchApiQueryInterface object to execute.
   */
  public function getFilterCondition(SearchApiQueryFilterInterface $filter, array $fields, SearchApiQueryInterface $query) {
    $conditions = array();
    // Get filters conjunction.
    $cond = $filter->getConjunction();

    // Go through filters and create an array with GSA condition strings.
    foreach ($filter->getFilters() as $f) {
      if (is_object($f)) {
        // If the filter is an object, process recursively.
        $c = $this->getFilterCondition($f, $fields, $query);
        if ($c) {
          $conditions[] = $c;
        }
      }
      else {
        // Filter has values, so we build condition in a GSA-friendly format.
        list($field, $value, $condition) = $f;
        if (!isset($fields[$field])) {
          throw new SearchApiException(t('Unknown field in filter clause: @field.', array('@field' => $field)));
        }

        // Check the condition type, to build the condition string for GSA.
        if ($condition === '=') {
          $conditions[] = $field . ':' . $value;
        }
        elseif ($condition === '!=' || $condition === '<>') {
          $conditions[] = '-' . $field . ':' . $value;
        }
      }
    }

    // Group the conditions based on the conjunction type.
    if (!empty($conditions)) {
      if ($cond === 'AND') {
        return '(' . implode('.', $conditions) . ')';
      }
      elseif ($cond === 'OR') {
        return '(' . implode('|', $conditions) . ')';
      }
    }

    return FALSE;
  }


  /**
   * Helper method to build the query to be sent to GSA.
   *
   * Adds filters to the query from Views and from Search API Index.
   *
   * @param SearchApiQueryInterface $query
   *   The search query for which to create and send the GSA query.
   * @param array $fields
   *   The internal field information to use.
   * @param string $entity_type
   *   Type of the entity, necessary to refine the search, based on Index data.
   */
  public function processQuery(SearchApiQueryInterface $query, array $fields, $entity_type, $no_limit = FALSE) {

    // This code is duplicated from the Google Appliance module.
    // We can't call that module's code, because params don't match.
    // Begin code ripped from google_appliance.
    // Grab module settings.
    $settings = $this->getSettings();

    // Get query options (like pager options: offset, limit, etc.).
    $options = $query->getOptions();

    // Response processing structure.
    // Curl arguments for the query.
    $search_query_data = array();
    // Processed response.
    $response_data = array();

    // Get terms (words) entered for search.
    $keys = $query->getKeys();
    if ($keys) {
      $words = implode(' ', array_intersect_key($keys, element_children($keys)));

      // Check if there's any filter added to the view and process it.
      $filter = $query->getFilter();
      if ($filter->getFilters()) {
        $conds = $this->getFilterCondition($filter, $fields, $query);
      }

      // Add filter by entity type, defined in Search API Index.
      $entity_filter = '(entity_type:' . $entity_type . ')';

      // Build cURL request.
      $search_query_data['gsa_host'] = check_plain($settings['hostname'] . '/search');
      $search_query_data['gsa_query_params'] = array(
        'site' => check_plain($settings['collection']),
        'oe' => 'utf8',
        'ie' => 'utf8',
        'getfields' => '*',
        'client' => check_plain($settings['frontend']),
        'start' => ($no_limit === FALSE) ? $options['offset'] : 0,
        'num' => ($no_limit === FALSE) ? $options['limit'] : 100,
        'filter' => check_plain($settings['autofilter']),
        'q' => $words,
        'output' => 'xml_no_dtd',
        'access' => 'p',
        // Add conditions from Views and Entity Type from Search API Index.
        'requiredfields' => ($conds) ? $conds . '.' . $entity_filter : $entity_filter,
      );

      // Alter request according to language filter settings.
      if (module_exists('locale') && $settings['language_filter_toggle']) {
        $search_query_data['gsa_query_params']['lr'] = _google_appliance_get_lr($settings['language_filter_options']);
      }

      // Allow implementation of hook_google_appliance_query_alter().
      drupal_alter('google_appliance_query', $search_query_data);

      // Build debug info in case we need to display it.
      if ($settings['query_inspection'] == '1') {
        $search_query_data['debug_info'][] = t('GSA host: @host', array('@host' => $search_query_data['gsa_host']));
        $search_query_data['debug_info'][] = t('Query Parameters: <pre>@qp</pre>',
            array('@qp' => print_r($search_query_data['gsa_query_params'], TRUE))
        );
      }

      $curl_options = array();

      // Allow implementation of hook_google_appliance_curl_alter().
      drupal_alter('google_appliance_curl', $curl_options);

      // Query the GSA for search results.
      $gsa_response = _curl_get(
          $search_query_data['gsa_host'],
          $search_query_data['gsa_query_params'],
          $curl_options,
          $settings['timeout']
      );

      // Check for handshake errors.
      if ($gsa_response['is_error'] == TRUE) {
        // Curl returned an error (can't connect).
        $response_data['error']['curl_error'] = $gsa_response['response'];
        // Displaying useful error messages depends upon use of the array key.
        // Curl_error - Actual error is replaced by hook_help messages.
        // @see google_appliance.theme.inc.
        // @see google_appliance_help().
      }
      else {
        // cURL gave us something back -> attempt to parse.
        $response_data = google_appliance_parse_device_response_xml($gsa_response['response'], FALSE);
      }
      // End code ripped from google_appliance.
      return $response_data;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Executes a search on the server represented by this object.
   *
   * If the service class supports facets, it should check for an additional
   * option on the query object:
   * - search_api_facets: An array of facets to return along with the results
   *   for this query. The array is keyed by an arbitrary string which should
   *   serve as the facet's unique identifier for this search. The values are
   *   arrays with the following keys:
   *   - field: The field to construct facets for.
   *   - limit: The maximum number of facet terms to return. 0 or an empty
   *     value means no limit.
   *   - min_count: The minimum number of results a facet value has to have in
   *     order to be returned.
   *   - missing: If TRUE, a facet for all items with no value for this field
   *     should be returned (if it conforms to limit and min_count).
   *   - operator: (optional) If the service supports "OR" facets and this key
   *     contains the string "or", the returned facets should be "OR" facets. If
   *     the server doesn't support "OR" facets, this key can be ignored.
   *
   * The basic principle of facets is explained quite well in the
   * @link http://en.wikipedia.org/wiki/Faceted_search Wikipedia article on
   * "Faceted search" @endlink. Basically, you should return for each field
   * filter values which would yield some results when used with the search.
   * E.g., if you return for a field $field the term $term with $count results,
   * the given $query along with
   *   $query->condition($field, $term)
   * should yield exactly (or about) $count results.
   *
   * For "OR" facets, all existing filters on the facetted field should be
   * ignored for computing the facets.
   *
   * @param SearchApiQueryInterface $query
   *   The SearchApiQueryInterface object to execute.
   *
   * @return array
   *   An associative array containing the search results, as required by
   *   SearchApiQueryInterface::execute().
   *   In addition, if the "search_api_facets" option is present on the query,
   *   the results should contain an array of facets in the "search_api_facets"
   *   key, as specified by the option. The facets array should be keyed by the
   *   facets' unique identifiers, and contain a numeric array of facet terms,
   *   sorted descending by result count. A term is represented by an array with
   *   the following keys:
   *   - count: Number of results for this term.
   *   - filter: The filter to apply when selecting this facet term. A filter is
   *     a string of one of the following forms:
   *     - "VALUE": Filter by the literal value VALUE (always include the
   *       quotes, not only for strings).
   *     - [VALUE1 VALUE2]: Filter for a value between VALUE1 and VALUE2. Use
   *       parantheses for excluding the border values and square brackets for
   *       including them. An asterisk (*) can be used as a wildcard. E.g.,
   *       (* 0) or [* 0) would be a filter for all negative values.
   *     - !: Filter for items without a value for this field (i.e., the
   *       "missing" facet).
   *
   * @throws SearchApiException
   *   If an error prevented the search from completing.
   */
  public function search(SearchApiQueryInterface $query) {
    $results = array();
    $index = $query->getIndex();
    $entity_type = $index->item_type;
    $fields = $index->options['fields'];

    $response = $this->processQuery($query, $fields, $entity_type);

    if ($response) {
      $results['results'] = array();
      if ($response['total_results'] > 0) {
        foreach ($response['entry'] as $key => $item) {
          if (isset($item['meta']['entity_type'])) {
            $entity_type = $item['meta']['entity_type'];
            $label_id = gsa_metatag_get_entity_identifier($entity_type);

            $id = $item['meta'][$label_id];
            $results['results'][$id] = array(
              'id' => $id,
              'score' => 1,
            );
          }
        }
      }

      // Total results from GSA response. Necessary to views pager.
      $results['result count'] = $response['total_results'];

      if ($results['result count'] > 1) {
        if ($query->getOption('search_api_facets')) {
          $results['search_api_facets'] = $this->getFacets($query, $fields);
        }
      }

      return $results;
    }
    else {
      return array(
        'result count' => 0,
      );
    }
  }


  /**
   * Computes facets for a search query.
   *
   * @param SearchApiQueryInterface $query
   *   The search query for which facets should be computed.
   * @param array $fields
   *   The internal field information to use.
   *
   * @return array
   *   An array of facets, as specified by the search_api_facets feature.
   */
  protected function getFacets(SearchApiQueryInterface $query, array $fields) {
    $index = $query->getIndex();
    $entity_type = $index->item_type;
    $response = $this->processQuery($query, $fields, $entity_type, TRUE);

    try {
      $ret = array();
      $facets = $query->getOption('search_api_facets');
      foreach ($facets as $key => $facet) {
        if (empty($fields[$facet['field']])) {
          $this->warnings[] = t('Unknown facet field @field.', array('@field' => $facet['field']));
          continue;
        }

        $ret[$key] = $this->getFacetFieldResults($facet, $response['entry'], $entity_type);
      }

      return $ret;
    }
    catch (PDOException $e) {
    }
  }


  /**
   * Build an array with facets in a format readable by Search API.
   *
   * @param array $facet
   *   Array with Facet data to compare with results to build final facet array.
   * @param array $results
   *   Array with results returned from GSA index.
   * @param string $entity_type
   *   Type of the entity, necessary to refine the search, based on Index data.
   *
   * @return array
   *   Facets values in a format readable by Search API.
   */
  protected function getFacetFieldResults($facet, $results, $entity_type) {
    $field = $facet['field'];
    $values = array();

    // Check results returned by GSA Index against current facet field.
    // Increases the counter if the result has a meta tag for the facet field.
    $processed = array();
    $label_id = gsa_metatag_get_entity_identifier($entity_type);
    foreach ($results as $result) {
      $id = $result['meta'][$label_id];
      if (!in_array($id, $processed)) {
        if (isset($result['meta'][$field])) {
          $value = $result['meta'][$field];
          if (isset($values[$value])) {
            $values[$value]++;
          }
          else {
            $values[$value] = 1;
          }
        }

        $processed[] = $id;
      }
    }

    // Go over values and build a facet array in a readable format by Facet API.
    foreach ($values as $filter => $count) {
      $ret[] = array(
        'count' => $count,
        'filter' => '"' . $filter . '"',
      );
    }

    return $ret;
  }


  /**
   * Determines whether GSA faceted service implementation supports a feature.
   *
   * @param string $feature
   *   The name of the optional feature.
   *
   * @return bool
   *   TRUE if this service knows and supports the specified feature. FALSE
   *   otherwise.
   */
  public function supportsFeature($feature) {
    $supported = array(
      'search_api_facets' => TRUE,
    );
    return isset($supported[$feature]);
  }

}
