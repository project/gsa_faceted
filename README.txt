GSA FACETED SEARCH
-----------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The GSA Faceted Search module adds the ability to create faceted search using
anything that integrates with the Facet API, including Views.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/gsa_faceted


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/gsa_faceted


REQUIREMENTS
------------

This module requires the following modules:

 * GSA Metatags (https://www.drupal.org/project/gsa_metatags)
 * Google Search Appliance (https://www.drupal.org/project/google_appliance)
 * Facet API (https://www.drupal.org/project/facetapi)
 * Search API (https://www.drupal.org/project/search_api)

This module also requires two patches to the Google Appliance module which are
found in the patches folder. These patches address these issues:
 * Allow GSA Faceted project to cache query (https://www.drupal.org/node/2617686)
 * Add basic support for faceting extensibility (https://www.drupal.org/node/2617690)



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Install and configure the google_appliance module.
 * Install and configure the gsa_metatag module.


CONFIGURATION
-------------

With thanks to the outline here:
http://envisioninteractive.com/drupal/drupal-7-views-with-faceted-filters-without-apachesolr/,
we’ve updated for the GSA Faceted Search below.

 * Configure user permissions in Administration » People » Permissions
 * Create and configure a Search API Search Server to use the GSA Faceted Service
 * Create and configure a Search Index to use the Server created above,
   check the fields you want to use as facets, and then enable the facets for
   those fields.
 * Configure GSA_Metatag and check those fields that you want to use as facets
 * Run cron to populate the index 
 * Create your view with the type of your Search Index created above, or other
   facet implementation.
 * Place the facet blocks as needed using Drupal's block Administration or
   Context module.
 


TROUBLESHOOTING
---------------

//TODO


MAINTAINERS
-----------

Current maintainers:

 * David Langarica (morgothz) - https://www.drupal.org/user/1072744/
 * Paul Schulzetenberg (unitoch) - https://www.drupal.org/user/251739

This project has been sponsored by:

 * University of Minnesota - Funding

   The University of Minnesota is Minnesota's research university. We change
   lives—through research, education, and outreach.

   https://twin-cities.umn.edu/

* ORIGIN EIGHT - Development

   Origin Eight is an expert Drupal & open source development agency and
   solutions partner specializing in full-service, strategic, integrated
   solutions for higher education, government, nonprofit organizations and
   private industry. Our top-notch team works collaboratively with clients
   to produce, support and maintain engaging and highly usable web experiences.

   http://origineight.net/
